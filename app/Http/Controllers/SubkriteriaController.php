<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subkriteria;
use App\Models\Kriteria;
use App\Models\Sc;

class SubkriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
// $subkriteria = Subkriteria ::all();

         $subkriteria  = Subkriteria::join('tbl_sc','tbl_subkriteria.faktor', '=', 'tbl_sc.id')
                                    ->select('tbl_sc.nama as faktor','tbl_subkriteria.subkriteria','tbl_subkriteria.bobot','tbl_subkriteria.id','tbl_subkriteria.kriteria as kriteria')
                                    // ->where('tbl_subkriteria.kriteria', '=', $id)
                                    ->get();

        
       
        
        return view('/sekertaris/subkriteria/index',compact('subkriteria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $subkriteria = Subkriteria::all(); 
        $kriteria    = Kriteria::all();
        $sc          = Sc::all();


        return view('sekertaris.subkriteria.create',compact('subkriteria','kriteria','sc','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subkriteria = new Subkriteria;
        $subkriteria->kriteria = $request->kriteria;
        $subkriteria->subkriteria = $request->subkriteria;
        $subkriteria->faktor = $request->faktor;
        $subkriteria->bobot = $request->bobot;      
        
        $subkriteria->save();   

        return redirect('kriteria')->with('status','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $subkriteria = Subkriteria::join('tbl_sc','tbl_subkriteria.faktor', '=', 'tbl_sc.id')
        //                             ->join('tbl_kriteria','tbl_subkriteria.kriteria','=','tbl_kriteria.id')                        
        //                             ->select('tbl_sc.nama as faktor','tbl_subkriteria.subkriteria','tbl_subkriteria.bobot',
        //                                     'tbl_kriteria.kriteria as kriteria')
        //                             ->where('tbl_subkriteria.id' , '=' , $id)
        //                             ->get();

        $kriteria = Kriteria::all();
        $sc = Sc::all();

$subkriteria = Subkriteria::all();
        $s = Subkriteria::where('id', '=', $id)->first();
        $k = Subkriteria::join('tbl_kriteria','tbl_subkriteria.kriteria','=','tbl_kriteria.id')
                         ->select('tbl_kriteria.kriteria as kriteria','tbl_kriteria.id')->first();
       
        $f = Subkriteria::join('tbl_sc','tbl_subkriteria.faktor','=', 'tbl_sc.id')
                          ->select('tbl_sc.nama as faktor','tbl_subkriteria.bobot','tbl_subkriteria.subkriteria')->first();

                  

        return view('sekertaris.subkriteria.update',compact('kriteria','s','k','f','sc','subkriteria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        

        $subkriteria = Subkriteria::find($id);

        $subkriteria->kriteria = $request->kriteria;
        $subkriteria->subkriteria = $request->subkriteria;
        $subkriteria->faktor = $request->faktor;
        $subkriteria->bobot = $request->bobot;      
        
        $subkriteria->save();   

        return redirect('kriteria')->with('status','Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Subkriteria::find($id); 
        $id->delete();
        return redirect('subkriteria')->with('status', 'Data berhasil update!');
    }
}
