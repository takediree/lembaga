<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class LoginController extends Controller
{
    //
    public function login()
    {
        return view('/login');
    }


    public function authenticate(Request $request)
    {
        

      
        try {
           
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {    

                session(['email' => $request->email]);

                return Redirect::to('/home');


            } else {

            
                session()->flash("pesan" , "Password / Email Salah");
                return Redirect::to('/');
            
            }



        }catch(\Exception $e){
            print_r($e->getMessage());
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
      }
}
