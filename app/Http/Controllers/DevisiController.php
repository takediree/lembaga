<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Devisi;

class DevisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devisi = Devisi::all();
        return view('sekertaris.devisi.index',compact('devisi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $devisi = Devisi::all();
        return view('sekertaris.devisi.create',compact('devisi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $devisi = new Devisi;
        $devisi->nama_devisi = $request->nama_devisi;
        $devisi->save();
        
        return redirect('devisi')->with('status', 'Data berhasil ditambahkan!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Devisi $devisi)
    {
        return view('sekertaris.devisi.update',compact('devisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $devisi = Devisi::find($id);
        $devisi->nama_devisi = $request->nama_devisi;
        $devisi->save();
        
        return redirect('devisi')->with('status', 'Data berhasil update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Devisi::find($id); 
        $id->delete();
        return redirect('devisi')->with('status', 'Data berhasil update!');
    }
}
