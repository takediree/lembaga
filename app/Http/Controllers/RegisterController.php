<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    public function register()
    {
        return view('/register');
    }

    public function create_register(Request $request){

       
        $user = new User();

        $user->name = $request->name;
        // $user->nis = $request->nis;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        $user->save();

        return redirect('/');


    }

}
