<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\Subkriteria;

class KriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kriteria = Kriteria::all();
        return view('sekertaris/kriteria/index',compact('kriteria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sekertaris/kriteria/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kriteria' => 'required',
            

        ]);

        $kriteria = new Kriteria;      
        $kriteria->kriteria = $request->kriteria;    
           
        
       $kriteria->save();
       return redirect('kriteria')->with('status', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $kriteria = Kriteria::where('id' , '=' , $id)->first();

        $subkriteria  = Subkriteria::join('tbl_sc','tbl_subkriteria.faktor', '=', 'tbl_sc.id')
                                    ->select('tbl_sc.nama as faktor','tbl_subkriteria.subkriteria','tbl_subkriteria.bobot','tbl_subkriteria.id as id')
                                    ->where('tbl_subkriteria.kriteria', '=', $id)
                                    ->get();

                                    

        return view('sekertaris/kriteria/show',compact('subkriteria','id','kriteria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        
        $kriteria = Kriteria::where('id' , '=' , $id)->first();
        
        return view('sekertaris.kriteria.update',compact('kriteria'));
    

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kriteria' => 'required',
           

        ]);

        $kriteria = Kriteria::find($id);      
        $kriteria->kriteria = $request->kriteria;    
        
        
       $kriteria->save();
       return redirect('kriteria')->with('status', 'Data berhasil ditambahkan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $k = Kriteria::find($id);
        $k->delete();
        return redirect('kriteria')->with('status', 'Data berhasil update!');
    }

   

}
