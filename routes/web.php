<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\KriteriaController;
use App\Http\Controllers\SubkriteriaController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ScController;
use App\Http\Controllers\ProfilematchingController;
use App\Http\Controllers\DevisiController;  
use App\Http\Controllers\AlternatifController;





Route::get('/', [LoginController::class, 'login'])->name('login');
Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/register', [RegisterController::class, 'register']);
Route::post('/register', [RegisterController::class, 'create_register']);


Route::post('/authenticate', [LoginController::class , 'authenticate']);

// Route::group(['middleware' => 'auth'], function () {

    
// });
    //sekertaris
    Route::get('/home', [HomeController::class, 'index']);

//kriteria
    Route::get('/kriteria', [KriteriaController::class, 'index']);
    Route::get('/kriteria/create', [KriteriaController::class, 'create']);
    Route::post('/kriteria', [KriteriaController::class, 'store']);
    Route::get('/kriteria/show/{id}', [KriteriaController::class, 'show']);
    Route::get('/kriteria/update/{kriteria}', [KriteriaController::class, 'edit']);
    Route::put('/kriteria/{kriteria}', [KriteriaController::class, 'update']);
    Route::get('/kriteria/delete/{id}', [KriteriaController::class, 'destroy']);


    Route::get('/subkriteria/update/{id}',[SubkriteriaController::class, 'edit']);
    Route::put('/subkriteria/{subkriteria}',[SubkriteriaController::class, 'update']);

    //subkriteria
    Route::get('/subkriteria',[SubkriteriaController::class, 'index']);
    Route::get('/subkriteria/create/{id}',[SubkriteriaController::class, 'create']);
    Route::post('/subkriteria',[SubkriteriaController::class, 'store']);
    Route::get('/subkriteria/delete/{id}',[SubkriteriaController::class, 'destroy']);
    
    //kategori
    Route::get('/kategori',[KategoriController::class, 'index']);
    Route::get('/kategori/create',[KategoriController::class, 'create']);
    Route::post('/kategori',[KategoriController::class, 'store']);
    
    //sc
    Route::get('/sc',[ScController::class, 'index']);
    Route::get('/sc/update/{sc}',[ScController::class, 'edit']);
    Route::put('/sc/{sc}',[ScController::class, 'update']);
    
    //devisi
    Route::get('/devisi',[DevisiController::class, 'index']);
    Route::get('/devisi/create',[DevisiController::class, 'create']);
    Route::post('/devisi',[DevisiController::class, 'store']);
    Route::get('/devisi/update/{devisi}',[DevisiController::class, 'edit']);
    Route::put('/devisi/{devisi}',[DevisiController::class, 'update']);
    Route::get('/devisi/delete/{id}',[DevisiController::class, 'destroy']);
    
    //alternatif
    Route::get('/alternatif',[AlternatifController::class, 'index']);
    Route::get('/alternatif/penilaian',[AlternatifController::class, 'penilaian']);
    
    //hitung
    Route::get('/profilematching',[ProfilematchingController::class, 'index']);
    

