<!DOCTYPE html>
<html lang="en">

<head>
 @include('sekertaris.template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
      
           @include('sekertaris.template.sidebar')

               @include('sekertaris.template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                <h1 class="h3 mb-2 text-gray-200">Penilaian</h1>
        
                <form method="post" action="{{ url("/alternatif/{alternatif}") }}">
                    @csrf
                    {{-- <input type="hidden" class="form-control" name=""  value=""> --}}
                
                
                    <div class="row">
                
                            <div class="card-body col-md-4 ">
                
                
                                <!-- <input type="text" class="form-control" name="id_kriteria" disabled  value=""> -->
                            
                
                                
                                    @foreach($subkriteria as $s)
                
                
                                            <div class="form-row">
                                                <div class="col-md-5">
                
                                                
                
                                                    
                                                            <span class="text-gray-200">{{$s->subkriteria}}</span>
                                                    
                                                    </div>
                
                                                        
                
                                                            <select class="form-control col-md-2" aria-label="Default select example" name="bobot" id="bobot">
                                                                <option selected></option>
                                
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                
                                
                                                            </select>
                
                                                
                
                                                      
                
                                            </div>
                                            <br>
                                    @endforeach
                
                
                            
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <input type="button" class="btn btn-primary" value="Kembali" onclick="history.back(-1)" />
                            </div>
                        
                        </div>
                
                    
                
                
                </form>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('sekertaris.template.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>

</body>

</html>