<!DOCTYPE html>
<html lang="en">

<head>
 @include('sekertaris.template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
      
           @include('sekertaris.template.sidebar')

               @include('sekertaris.template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                <h1 class="h3 mb-2 text-gray-200">Tambah Kategori</h1>
        
                <form method="post" action="{{url('kategori')}}">
                    @csrf
                    <div class="card-body col-md-4 ">
                        <div class="form-group">
                            <label for="kategori" > Kategori </label>
                            <input type="text" class="form-control @error('kategori') is-invalid @enderror" id="kategori" placeholder=" Nama Kategori" name="kategori"  value="{{ old('kategori') }}"required>
                           
                            @error('kelas')
                                <div class="invalid-feedback">
                                    inputan salah
                                </div>
                             @enderror
                        
                        </div>
                        <div class="form-group">
                            <label for="bobot" > Bobot </label>
                            <input type="text" class="form-control @error('bobot') is-invalid @enderror" id="bobot" placeholder=" Bobot" name="bobot"  value="{{ old('bobot') }}"required>
                            
                            @error('bobot')
                                <div class="invalid-feedback">
                                    Gunakan angka
                                </div>
                            @enderror
                        
                        </div>
                        <div class="form-group">
                            <label for="bobot_akhir" > Bobot akhir </label>
                            <input type="text" class="form-control @error('bobot_akhir') is-invalid @enderror" id="bobot_akhir" placeholder=" Bobot akhir" name="bobot_akhir"  value="{{ old('bobot_akhir') }}"required>
                        
                            @error('bobot_akhir')
                                <div class="invalid-feedback">
                                    Gunakan angka
                                </div>
                            @enderror

                        </div>
                           
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <!-- /.card-body -->



                </form>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('sekertaris.template.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>

</body>

</html>