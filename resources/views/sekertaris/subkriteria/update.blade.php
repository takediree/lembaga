<!DOCTYPE html>
<html lang="en">

<head>
 @include('sekertaris.template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
      
           @include('sekertaris.template.sidebar')

               @include('sekertaris.template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                <h1 class="h3 mb-2 text-gray-200">Update Subkriteria</h1>
        
                <form method="post" action="{{url("subkriteria/".$s->id)}}">
                    @csrf
                    @method('PUT')
                    <div class="card-body col-md-6 ">
                        <select class="form-control" aria-label="Default select example" name="kriteria" id="kriteria"  >
                            <option selected  >{{$k->kriteria}}</option>
                            @foreach($kriteria as $k)
                            
                                <option value="{{$k->id}}">{{$k->kriteria}}</option>
                                

                                @endforeach
                            </select>
                            <br>
                            <select class="form-control" aria-label="Default select example" name="faktor" id="faktor"  >
                                <option selected disabled >{{$f->faktor}}</option>
                                    @foreach($sc as $s)

                                        <option value={{$s->id}}>{{$s->nama}}</option>
                            
                                    @endforeach
                            </select>
                            <br>
                            <div class="form-group">
                                {{-- <label for="nama_kriteria"> Kriteria </label> --}}
                                <input type="text" class="form-control" id="subkriteria" placeholder=" Nama Subriteria" name="subkriteria" value="{{$f->subkriteria}} "required>
                            </div>
                            <select class="form-control" aria-label="Default select example" name="bobot" id="bobot">
                                <option selected>{{$f->bobot}}</option>

                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>

                            </select>
                      <br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{url('kriteria')}}" class="btn btn-primary">Kembali</a>
                    </div>
                    <!-- /.card-body -->



                </form>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('sekertaris.template.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>

</body>

</html>