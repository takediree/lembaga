-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 27, 2021 at 08:06 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lembaga`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(29, '2014_10_12_000000_create_users_table', 1),
(30, '2014_10_12_100000_create_password_resets_table', 1),
(31, '2019_08_19_000000_create_failed_jobs_table', 1),
(32, '2021_07_12_041006_create_tbl_kriteria', 1),
(33, '2021_07_25_125422_create_tbl_subkriteria_table', 1),
(34, '2021_08_14_010644_create_tbl_kategori_table', 1),
(35, '2021_08_23_120653_create_tbl_sc_table', 1),
(36, '2021_08_24_122632_create_tbl_devisi_table', 2),
(37, '2021_08_27_152252_create_tbl_alternatif_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alternatif`
--

CREATE TABLE `tbl_alternatif` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_devisi` int(11) NOT NULL,
  `id_subkriteria` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_devisi`
--

CREATE TABLE `tbl_devisi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_devisi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_devisi`
--

INSERT INTO `tbl_devisi` (`id`, `nama_devisi`, `created_at`, `updated_at`) VALUES
(1, 'Bidang Minat dan Bakat', '2021-08-24 12:50:41', '2021-08-27 17:27:42'),
(3, 'Bidang Pendidikan Dan Pelatihan', '2021-08-24 15:07:46', '2021-08-24 15:14:15'),
(4, 'Bidang Kerohanian', '2021-08-24 15:14:38', '2021-08-24 15:14:38'),
(5, 'Bidang Kewirausahaan', '2021-08-24 15:14:57', '2021-08-24 15:14:57'),
(6, 'Bidang Keorganisasian', '2021-08-24 15:15:22', '2021-08-24 15:15:22'),
(7, 'Bidang Kesekretariatan', '2021-08-24 15:15:39', '2021-08-24 15:16:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` double NOT NULL,
  `bobot_akhir` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kriteria`
--

CREATE TABLE `tbl_kriteria` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kriteria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_kriteria`
--

INSERT INTO `tbl_kriteria` (`id`, `kriteria`, `created_at`, `updated_at`) VALUES
(1, 'Aspek Prestasi Kriteriaa', '2021-08-23 13:17:54', '2021-08-26 16:44:31'),
(4, 'Aspek Ketaatan', '2021-08-23 14:23:06', '2021-08-24 15:29:54'),
(6, 'Aspek Kerjasama', '2021-08-24 12:16:52', '2021-08-24 15:30:04'),
(7, 'Aspek Tanggung Jawab', '2021-08-24 12:23:05', '2021-08-24 15:18:20'),
(8, 'Aspek Kepemimpinan', '2021-08-24 15:18:39', '2021-08-24 15:18:39'),
(9, 'Aspek Kepribadian', '2021-08-24 15:18:59', '2021-08-24 15:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sc`
--

CREATE TABLE `tbl_sc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_sc`
--

INSERT INTO `tbl_sc` (`id`, `nama`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 'Core Faktor', 60, NULL, '2021-08-25 15:11:01'),
(2, 'Secondary Faktor', 40, NULL, '2021-08-25 15:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subkriteria`
--

CREATE TABLE `tbl_subkriteria` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kriteria` int(11) NOT NULL,
  `subkriteria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faktor` int(11) NOT NULL,
  `bobot` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_subkriteria`
--

INSERT INTO `tbl_subkriteria` (`id`, `kriteria`, `subkriteria`, `faktor`, `bobot`, `created_at`, `updated_at`) VALUES
(1, 1, 'bacot', 1, 4, '2021-08-23 13:19:41', '2021-08-25 17:30:30'),
(17, 1, 'Taat Dalam Aturan ADRT', 1, 4, '2021-08-25 14:26:03', '2021-08-25 17:24:47'),
(18, 4, 'Taat Terhadap Program Kerja', 70, 4, '2021-08-25 14:26:43', '2021-08-25 14:26:43'),
(19, 4, 'Mengikuti Kegiatan Rutin Dikampus', 30, 5, '2021-08-25 14:27:21', '2021-08-25 14:27:21'),
(20, 6, 'Mampu Bekerja Dengan Tim', 70, 4, '2021-08-25 14:28:03', '2021-08-25 14:28:03'),
(21, 6, 'Mampu/bersedia Menerima Kritik Dan Saran Untuk Pengembangan', 30, 5, '2021-08-25 14:29:14', '2021-08-25 14:29:14'),
(22, 7, 'Mengutamakan Kepentingan Lembaga Dari Pada Kepentingan Sendiri, Orang Lain Atau Golongan', 30, 4, '2021-08-25 14:34:21', '2021-08-25 14:34:21'),
(23, 7, 'Berai Mengambil Resiko Dari Keptusan Yang Dibuat', 70, 5, '2021-08-25 14:35:34', '2021-08-25 14:35:34'),
(24, 7, 'Tidak Menyalahkan Wewenang Dan Tanggung Jawab', 70, 5, '2021-08-25 14:36:14', '2021-08-25 14:36:14'),
(25, 7, 'Melaporkan Hasil Kerja Tim Kepad BLM Sesuai Dengan Keadaan Yang Sebenarnya', 70, 5, '2021-08-25 14:37:12', '2021-08-25 14:37:12'),
(26, 8, 'Mampu Mengambil Keputusan Dengan Tepat Dan Cepat', 70, 4, '2021-08-25 14:38:37', '2021-08-25 14:38:37'),
(27, 8, 'Mampu Mengkomunikasikan Pekerja Dengan Sistematis', 70, 4, '2021-08-25 14:40:06', '2021-08-25 14:40:06'),
(28, 8, 'Mampu Menentukan Prioritas Kerja Dengan Tepat', 70, 4, '2021-08-25 14:40:58', '2021-08-25 14:40:58'),
(29, 8, 'Mampu Bertindak Tegas Dan Tidak Memihak', 70, 5, '2021-08-25 14:41:44', '2021-08-25 14:41:44'),
(30, 8, 'Mampu Mengembangkan Kerjasama', 30, 5, '2021-08-25 14:42:22', '2021-08-25 14:42:22'),
(31, 8, 'Memperhatikan Dan Mendorong Kemajuan Lembaga', 70, 5, '2021-08-25 14:48:38', '2021-08-25 14:48:38'),
(32, 8, 'Memiliki Sifat Mengayomi', 30, 5, '2021-08-25 14:50:58', '2021-08-25 14:50:58'),
(34, 1, 'tt', 1, 3, '2021-08-25 17:18:38', '2021-08-25 17:18:38'),
(35, 1, 'zzz', 1, 3, '2021-08-26 15:01:43', '2021-08-26 15:01:43'),
(36, 4, 'Cakap Dalam Penguasaan Tugas', 2, 3, '2021-08-26 15:02:13', '2021-08-26 15:02:13'),
(37, 1, 'Cakap', 1, 3, '2021-08-26 17:23:20', '2021-08-26 17:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `role`, `created_at`, `updated_at`) VALUES
(2, 'takdir', 't@gmail.com', NULL, '$2y$10$1J0L4quFjS12Kgtj1Q/DquseJ3A/QeTPzoXECpmdt9TJcRgyJRNEK', NULL, 1, '2021-08-26 13:57:32', '2021-08-26 13:57:32'),
(4, 'anha', 'nuramanah@gmail.com', NULL, '$2y$10$6E1OacwG049yU1Ctk.ScieqiqTD00hohd7ug/dFh3alJoFqlJaAxm', NULL, 1, '2021-08-27 15:26:47', '2021-08-27 15:26:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_alternatif`
--
ALTER TABLE `tbl_alternatif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_devisi`
--
ALTER TABLE `tbl_devisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kriteria`
--
ALTER TABLE `tbl_kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sc`
--
ALTER TABLE `tbl_sc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subkriteria`
--
ALTER TABLE `tbl_subkriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_alternatif`
--
ALTER TABLE `tbl_alternatif`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_devisi`
--
ALTER TABLE `tbl_devisi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_kriteria`
--
ALTER TABLE `tbl_kriteria`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_sc`
--
ALTER TABLE `tbl_sc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_subkriteria`
--
ALTER TABLE `tbl_subkriteria`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
